FROM alpine:3.20

LABEL maintainer="Unai IRIGOYEN <u.irigoyen@gmail.com>"

RUN apk --update add \
    # Add Ansible and related packages
    ansible openssh sshpass \
    # Add utilities
    git py3-pip && \
    rm -rf /var/cache/apk/*
